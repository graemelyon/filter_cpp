// Main.cc

#include "white_noise.h"
#include "filter.h"
#include "buffer.h"
#include "sound_output_stream.h"

int main(int argc, char *argv[]) {
	
// Create buffer
Buffer b(88200);

// Call constructor to generate noise
White_noise white(1.0, b);	// Generates White noise
Filter filtered(440, 44100, b);	// Filters signal


// Set up soundcard
Sound_output_stream sound(44100, 16, 2);
// Output values to soundcard
sound.sound_write(b);

}