// white_noise.c

#include <cstdlib>
#include "White_noise.h"
#include "buffer.h"

White_noise::White_noise(double a, Buffer& b){			// Contrsuctor
	
	int buffsize = b.getsize();
	for(int n=0; n<buffsize; n++){	
		b.mybuffer[n] = (static_cast<double>(std::rand()) * 2 / RAND_MAX) - 1;	// Random samples between -1 & 1
	}			
}

White_noise::~White_noise() {}