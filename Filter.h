// Filter.h

#ifndef FILTER_H
#define FILTER_H

class Buffer;

class Filter {

private:
	double r_p;
	double r_z;
	double y_n;
	double y_1;
	double y_2;
	double x_n;
	double x_1;
	double x_2;
public:
	Filter(int, int, Buffer&);		// Constructor
	~Filter();		// Destructor
};

#endif