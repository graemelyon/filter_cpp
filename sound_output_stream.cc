// Sound Output Stream.cc

#include <iostream>
#include <fcntl.h>
#include <linux/soundcard.h>
#include <sys/ioctl.h>
#include <stdlib.h>

#include "buffer.h"
#include "sound_output_stream.h"

// If a parameter is out of range for ioctl the driver will set it to the closest possible value!

Sound_output_stream::Sound_output_stream(int rate, int size, int chan) {		// Constructor

//	Open soundcard
	if((dspfd = open("/dev/dsp", O_WRONLY)) == -1) {
		std::cerr << "Open failed" << std::endl;			// If soundcard cannot be opened report error
		exit(1);
}

//	Sets sample rate
	if((ioctl(dspfd, SOUND_PCM_WRITE_RATE, &rate)) == -1) {
		std::cerr << "Unable to set sample rate" << std::endl;		// If sample rate cannot be set report error
		exit(1);
}
	samplerate = rate;
	std::cout << "Sample rate set at : " << samplerate << " Hz" << std::endl;


//	Sets sample size
	if((ioctl(dspfd, SOUND_PCM_WRITE_BITS, &size)) == -1) {
		std::cerr << "Unable to set sample size" << std::endl;		// If sample size cannot be set report error
		exit(1);
}
	samplesize = size;
	std::cout << "Sample size set to : " << samplesize << " bits" << std::endl;


//	Sets number of channels
	if((ioctl(dspfd, SOUND_PCM_WRITE_CHANNELS, &chan)) == -1) {
		std::cerr << "Unable to set number of channels" << std::endl;	// If channels cannot be set report error
		exit(1);
}
	channels = chan;
	std::cout << "Number of channels set to : " << channels << std::endl;

};

Sound_output_stream::~Sound_output_stream() {				// Destructor
	close(dspfd);							// Closes soundcard
};

void Sound_output_stream::sound_write(Buffer& b) {
	int size = b.getsize();
	short *samples = new short[size];			// Create a new array to store the scaled samples
					
	for (int i=0; i<=size; i++) {
		double scaled = b.mybuffer[i] * ((1 << (samplesize-1)) - 1);	// Scale output relevant to sample size	
		samples[i] = static_cast<short> (scaled);			// Cast each sample to a short and store in new array samples
	}

	write(dspfd, samples, sizeof(short) * size);						// Write values to soundcard. At last!!
	delete[] samples;
};

