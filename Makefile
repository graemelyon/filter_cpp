PROGNAME := makesoundplease
CXXSRCS := main.cc White_noise.cc Filter.cc buffer.cc sound_output_stream.cc
CXXFLAGS += -g

CXXOBJS := $(CXXSRCS:.cc=.o)

$(PROGNAME): $(CXXOBJS)
	$(LINK.cc) -o $@ $(CXXOBJS)

.PHONY: clean
clean:
	$(RM) \#*\# *.o *~ core .depend $(PROGNAME)

.PHONY: depend
depend:
	$(CPP) -M $(CXXSRCS) > .depend

ifeq (.depend,$(wildcard .depend))
include .depend
endif