// white_noise.h

#ifndef WHITE_NOISE_H
#define WHITE_NOISE_H

class Buffer;

class White_noise {

private:

public:
	White_noise(double, Buffer&);		// Constructor
	~White_noise();				// Destructor
};

#endif