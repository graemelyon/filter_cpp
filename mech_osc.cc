// Mech_osc.c

#include <cmath>
#include "mech_osc.h"
#include "buffer.h"

Mech_osc::Mech_osc(double a, int freq, int rate, Buffer& b) {			// Contrsuctor
	
	int r_p = 0.999;
	int r_p = 0.95;

	int buffsize = b.getsize();
	double freqconst = a * 2 * r_p * std::cos(2*M_PI*freq/rate);	

	y_2 = 0;
	y_1 = std::sin(2*M_PI*freq/rate);

	for(int i = 0; i < buffsize; i++){
		y_n = freqconst*y_1 - y_2;		// Calculate sample
		b.mybuffer[i] = y_n;			// Write to buffer
						// Shift samples along
		y_2 = y_1;
		y_1 = y_n;

	}
}

Mech_osc::~Mech_osc() {			// Destructor
};