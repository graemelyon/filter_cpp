// Sound Output Stream.h

#ifndef SOUND_OUTPUT_STREAM_H
#define SOUND_OUTPUT_STREAM_H

class Buffer;

class Sound_output_stream {

private:
	int dspfd;				// Soundcard file descriptor
	int samplesize;
	int channels;
	int samplerate;

public:
	Sound_output_stream(int, int, int);	// Constructor
	~Sound_output_stream();			// Destructor
	void sound_write(Buffer&);		// Write samples to soundcard
};

#endif