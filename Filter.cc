// Filter.cc

#include <cmath>
#include "White_noise.h"
#include "buffer.h"
#include "Filter.h"

Filter::Filter(int freq, int rate, Buffer& b) {			// Contrsuctor
	
	double largest = 0;	
	r_p = 0.99999;
	r_z = 0.95;

	int buffsize = b.getsize();
	double freqconst = 2 * std::cos(2*M_PI*freq/rate);	

	for(int i = 0; i < buffsize; i++){
		x_n = b.mybuffer[i];

		y_n = - (x_1 * r_z * freqconst) + (x_2 * r_z * r_z) + (y_1 * r_p * freqconst) - (y_2 * r_p * r_p);	// Calculate sample

		if(std::abs(y_n) > largest) {
			largest = std::abs(y_n);
		}

		b.mybuffer[i] = y_n;		// Write to buffer
						
		y_2 = y_1;			// Shift samples along
		y_1 = y_n;
		x_2 = x_1;
		x_1 = x_n;
	}

	for(int i = 0; i < buffsize; i++){
		b.mybuffer[i] /= largest;
	}
}

Filter::~Filter() {			// Destructor
};