// Mech_osc.h

#ifndef MECH_OSC_H
#define MECH_OSC_H

class Buffer;

class Mech_osc {

private:
	double y_n;
	double y_1;
	double y_2;
public:
	Mech_osc(double, int, int, Buffer&);		// Constructor
	~Mech_osc();		// Destructor
};

#endif